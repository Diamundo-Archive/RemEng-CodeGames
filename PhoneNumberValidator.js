function validate ( /* int or string */ phoneNumber ) {
	Debug("Testing: " + phoneNumber);
	var numString = phoneNumber.toString();
	
	if(numString.length != 7 && numString.length != 8) {
		Debug(numString + ' has length ' + numString.length);
		return false;
		// number should have 7 digits, optionally one '-' as 4th character for a total of 8
	}
	
	if(numString.length == 7) {
		Debug("Number(" + numString + ") = " + Number(numString) + " && Number(" + numString + ").length = " + Number(numString).toString().length );
		return !isNaN(Number( numString )) && Number(numString).toString().length == 7;
		// if it's missing an '-', the phonenumber should be parseable as a Number.
		// Edge case: Date() will return a parseable value that represents the number of milliseconds since midnight Jan 1 1970 up to the specified Date.
	} else if(numString.length == 8) {
		var j;
		for(j=0; j<numString.length; j++){
			Debug("Testing digit " + j + " = " + numString[j] + " && Number("+numString[j]+") = " + Number(numString[j]) );

			if(! ( 
				!isNaN( Number(numString[j])) //this digit should be parseable as a Number
				||
				(j==3 && numString[j] == '-') //the 4th character (0-based index of 3) should be the '-'
			)){
				return false;
			}
		}
		
	}
	
	return true; //end case if it doesn't fail any of the tests and is considered a good number.

}

function testPhoneNumber( number ){
	console.log("Phone number " + number + " is " + (validate(number) ? "" : "not ") + "a valid phonenumber.");
}

function aboutPhoneNumberValidator(){
	console.log("== PhoneNumberValidator ==");
	console.log("Function to check if any given input is a valid phonenumber in the format <3 digits><optional dash><4 digits>.");
	console.log("If the supplied number has a dash, put it between '' quotes, otherwise Javascript will treat it as a calculation.");
	console.log("Example: testPhoneNumber( 123-4567 ) will return 'Phone number -4444 is not a valid phonenumber.' ");
	console.log("Use: testPhoneNumber( number );");
}

function PhoneNumberValidator(){
	console.log("");
	console.log("Test Cases:");
	var phoneNumbers = ['123-4567', '1234567', '123-456a', '123a567']; //testcases
	
	for(var i=0; i<phoneNumbers.length; i++) {
		testPhoneNumber( phoneNumbers[i] );
	}

}